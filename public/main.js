const cards = document.querySelectorAll(".smallcard");
const container = document.querySelector(".container");
const input = document.getElementById("searchinput");
const loader = document.querySelector(".loader");
const close = document.querySelector(".close");
let timer,para = 0;
let values = [];

let createfunc = ele => {
    let smalldiv = document.createElement("div");
    let image = document.createElement("img");
    let details = document.createElement("div");
    let p1 = document.createElement("p");
    let p2 = document.createElement("p");
    smalldiv.setAttribute("class","smallcard");
    image.src = "./icons8-male-user-96.png";
    details.setAttribute("class","details");
    p1.textContent = ele.username + " " + ele.handle;
    p2.textContent = ele.work + ". " + ele.bio;
    details.appendChild(p1);
    details.appendChild(p2);
    smalldiv.appendChild(image);
    smalldiv.appendChild(details);
    container.appendChild(smalldiv);
    smalldiv.addEventListener("click",e => {
        let str = e.currentTarget.children[1].children[0].innerText;
        let arr = str.split("");
        let x = arr.indexOf("@");
        let finalstr = arr.slice(x).join("");
        window.location.href = "/profile?handle=" + finalstr;
    });
}

if(sessionStorage.getItem("searchres") != null){
    let extra = sessionStorage.getItem("searchres");
    JSON.parse(extra).forEach(ele => {
        createfunc(ele);
    });
}

let callfunc = val => {
    let data = {
        username: val
    }
    clearTimeout(timer);
    timer = setTimeout(() => {
        loader.classList.remove("none");
        close.classList.add("none");
        fetch("/retrieve",{
            method: "post",
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        }).then(response => response.json())
        .then(data => {
            para = 0;
            values = [];
            container.innerHTML = "";
            loader.classList.add("none");
            close.classList.remove("none");
            if(data.length != 0){
                data.forEach(ele => {
                    createfunc(ele);
                    values.push(ele);
                });
                sessionStorage.setItem("searchres",JSON.stringify(values));
            } else {
                const nores = document.createElement("div");
                nores.textContent = "No results";
                nores.setAttribute("class","nores");
                container.appendChild(nores);
            }
        });
    },500);
}

input.addEventListener("keyup",() => {
    if(input.value != ""){
        callfunc(input.value);
    }
});

container.addEventListener("scroll",() => {
    if(container.clientHeight + container.scrollTop == container.scrollHeight){
        if(para == 0){
            loader.classList.remove("none");
            close.classList.add("none");
            fetch("/more")
            .then(response => response.json())
            .then(data => {
                loader.classList.add("none");
                close.classList.remove("none");
                values = JSON.parse(sessionStorage.getItem("searchres"));
                if(data.length != 0){
                    data.forEach(ele => {
                        createfunc(ele);
                        values.push(ele);
                    });
                    sessionStorage.setItem("searchres",JSON.stringify(values));
                } else {
                    para = 1;
                }
            });
        }
    }
});

close.addEventListener("click",() => {
    input.value = "";
});