const express = require("express");
const app = express();
const port = process.env.PORT || 4000;
const data = require("./profiles.json");
let no = 8;
let len = 0;
let arr,user;

app.use(express.json());
app.use(express.static("public"));
app.set("view engine","ejs");

app.get("/",(req,res) => {
    res.sendFile(__dirname + "/public/main.html");
});

app.post("/retrieve",(req,res) => {
    arr = [];
    if(req.body.username != "all"){
        data.forEach(each => {
            if(each.username.match(req.body.username)){
                arr.push(each);
            }
        });
        len = arr.length;
    } else {
        data.forEach(each => {
            arr.push(each);
        });
    }
    len = arr.length;
    no = 0;
    if(arr.length > 8){
        no = 8;
        setTimeout(() => {
            res.send(arr.slice(0,8));
        },4000);
    } else if(arr.length > 0){
        setTimeout(() => {
            res.send(arr);
        },4000);
    } else {
        setTimeout(() => {
            res.send([]);
        },4000);
    }
});

app.get("/more",(req,res) => {
    if(no > 0 && no < len){
        if(no + 8 <= len){
            setTimeout(() => {
                res.send(arr.slice(no,no+8));
                no = no+8;
            },4000);
        } else {
            setTimeout(() => {
                res.send(arr.slice(no,len));
                no = len;
            },4000);
        }
    } else {
        setTimeout(() => {
            res.send([]);
        },4000);
    }
});

app.get("/profile",(req,res) => {
    data.forEach(ele => {
        if(ele.handle == req.query.handle){
            user = ele;
        }
    })
    res.render("profile",{ data: user });
});

app.listen(port,() => console.log(`running on ${port}`));